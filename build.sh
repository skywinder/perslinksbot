pip install --target ./package -r requirements.txt

cd package
zip -r ../deployment-package.zip .

cd -
zip -g deployment-package.zip lambda_function.py