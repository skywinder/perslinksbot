import boto3
from boto3.dynamodb.conditions import Key
import logging
import telebot
import os
import base64
from datetime import datetime
from uuid import uuid4

TOKEN = os.environ['TOKEN']

logger = logging.getLogger()
logger.setLevel(logging.INFO)

bot = telebot.TeleBot(TOKEN)
table = boto3.resource("dynamodb").Table("personal_links_bot_table")

categories = {
    "fun": "Развлечение",
    "long": "Вчитаться",
    "arch": "Архитектура",
}

def s64encode(text):
    return base64.b64encode(text.encode("utf-8")).decode("utf-8")


def s64decode(text):
    return base64.b64decode(text.encode("utf-8")).decode("utf-8")


def process_message(message):
    logger.info("Message %s", message.text)

    if message.text.startswith("http"):
        link = message.text.strip()

        link_id = save_link(message.from_user.id, link)

        markup = telebot.types.InlineKeyboardMarkup()
        for category_slug, category_name in categories.items():
            markup.add(telebot.types.InlineKeyboardButton(category_name, callback_data=f"addlink_{ category_slug }_{ link_id }"))

        bot.reply_to(message, "Выберите категорию", reply_markup=markup)

    elif message.text == "/get":
        msg_categories(message.from_user.id)

    else:
        bot.reply_to(message, "You said " + message.text)


def msg_categories(chat_id):
    stats = get_stats()

    markup = telebot.types.InlineKeyboardMarkup()
    for category_slug, category_name in categories.items():
        markup.add(telebot.types.InlineKeyboardButton(f"{ category_name } ({ stats.get(category_slug, 0) })", callback_data=f"get_{ category_slug }"))

    bot.send_message(chat_id, "Выберите категорию", reply_markup=markup)


def addlink_callback(call):
    logger.info("Command %s", call.data)

    if call.data.startswith("addlink"):
        _, category_slug, link_id = call.data.split("_")

        set_link_category(link_id, category_slug)

        bot.send_message(call.from_user.id, "Link saved")

    elif call.data.startswith("get"):
        _, category_slug = call.data.split("_")

        record = get_link(category_slug)["Items"][0]

        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton("Ready", callback_data=f"dellink_{ record['id'] }"))
        bot.send_message(call.from_user.id, record["link"], reply_markup=markup)

    elif call.data.startswith("dellink"):
        _, link_id = call.data.split("_")
        delete_link(link_id)

        bot.send_message(call.from_user.id, "Link was deleted")

        msg_categories(call.from_user.id)

    else:
        bot.send_message(call.from_user.id, "Unkwnow command")


def get_stats():
    stats = {}
    for category_slug in categories:
        result = table.query(
            IndexName='category_slug-created_at-index',
            Select='COUNT',
            KeyConditionExpression=Key('category_slug').eq(category_slug),
        )

        stats[category_slug] = result["Count"]

    return stats


def save_link(user, link):
    link_id = str(uuid4()).replace("-", "")

    response = table.put_item(
        Item = {
            "id": link_id,
            "link": link,
            "user": user,
            "created_at": str(datetime.now()),
            "category_slug": "_empty",
        }
    )

    return link_id


def set_link_category(link_id, category_slug):
    response = table.update_item(
        Key={
            'id': link_id
        },
        AttributeUpdates={
            'category_slug': {
                'Value': category_slug,
                'Action': 'PUT',
            }
        },
    )


def get_link(category_slug):
    record = table.query(
        IndexName='category_slug-created_at-index',
        Select='ALL_ATTRIBUTES',
        KeyConditionExpression=Key('category_slug').eq(category_slug),
        Limit=1,
    )

    logger.info("Found link %r", record)

    return record


def delete_link(link_id):
    table.delete_item(
        Key={
            'id': link_id
        },
    )


def lambda_handler(event, context):
    logger.info("MESSAGE %r", event['body'])

    update = telebot.types.Update.de_json(event['body'])

    if update.message:
        process_message(update.message)

    elif update.callback_query:
        addlink_callback(update.callback_query)

    else:
        logger.warning("Unknown message type %r", update)

    return {'statusCode': 200}
